$(document).ready(function() {

		var sagUrl = "http://seeds.treeandfield.com/plant.php";
		
		var domain = "indigoshade.auth0.com";
		
		var options = {
			"container": 'login-container-ss',
			"responseType":"token",
			"autoclose":true,
			"focusInput":false,
			"popup":false,
			"socialBigButtons":true,
			"primaryColor":'#CCCCCC',
			"connections":["facebook","twitter"]
		 };
		
		
	    // create lock  ------------------------------------------------- //
	    
		var lock = new Auth0LockPasswordless(clientID, domain);
		lock.socialOrMagiclink(options);
		
		// update buttons
		setTimeout(function(){
			$('.auth0-lock-social-button-text span:eq(0), .auth0-lock-social-button-text span:eq(2)').html('Sign Up with ');
		}, 100);
		
		
		// get profile or error
		var hash = lock.parseHash(window.location.hash);
		
		if (hash && hash.error) {
		    alert('There was an error: ' + hash.error + '\n' + hash.error_description);
		} 
		else if (hash && hash.id_token) {
		    //use id_token for retrieving profile.
		    localStorage.setItem('id_token', hash.id_token);
		    //retrieve profile
		    lock.getProfile(hash.id_token, function (err, profile) {
		      if (err){
		        console.log(err);
		      } else {
			      
		        profile.product = product;
		        
		        var add = {
					"id":"",
					"action":"add",
					"database":"user_funnel",
					"doc":profile
			    }
		        
		        var win = function(rslt){
				 	console.log('win');
				 	console.log(rslt);
				 	
				 	$("#userfunnel").addClass('thanks');
				 	
				};
				
				var fail = function(rslt){
					console.log('fail');
					console.log(rslt);
				};
				
				$.ajax({
					type:"post",
					url: sagUrl,
					data: JSON.stringify(add),
					success: win,
					error: fail
				});

		        
		        // --- 				
		      }
		    });
		  }
		
		
	});